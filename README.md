# Todo List w React and local storage
Input your today activity

## Deployment
Deploy with vercel app
```
  https://todo-react-localstorage.vercel.app
```

## Documentation

### Add Todo
![Add](https://raw.githubusercontent.com/setyawannnIMG/todolistcrudreactlocalstorage/main/add%20todo.png)

### Edit Todo
![Add](https://raw.githubusercontent.com/setyawannnIMG/todolistcrudreactlocalstorage/main/edit%20todo.png)

### Todo done
![Add](https://raw.githubusercontent.com/setyawannnIMG/todolistcrudreactlocalstorage/main/done%20todo.png)

### Warning delete
![Add](https://raw.githubusercontent.com/setyawannnIMG/todolistcrudreactlocalstorage/main/warning%20delete%20todo.png)

### Delete todo
![Add](https://raw.githubusercontent.com/setyawannnIMG/todolistcrudreactlocalstorage/main/delete%20todo.png)

## Author

- Prayoga Adi Setyawan [@setyawannn](https://www.gitlab.com/setyawannn)
- Reyhan Marsalino Diansa [@ReyhanDiansa](https://gitlab.com/ReyhanDiansa)
- Zhidan Marties Alfareza [@lakyulakyu](https://gitlab.com/lakyulakyu)
- Zaskia Rizky Raichand [@zaskiakiaa802](https://gitlab.com/zaskiakiaa802)


## Technology
- HTML 5
- CSS 3
- React Js
- Node Js
- Moment Js
- Sweetalert 2
- Bootstrap 5.2

<p align="center">
    <picture>
      <source media="(prefers-color-scheme: white)" srcset="https://github.com/setyawannn/TEFA_team-portfolio/blob/main/assets/images/logo-icon.png?raw=true">
      <img src="https://github.com/setyawannn/TEFA_team-portfolio/blob/main/assets/images/logo-icon.png?raw=true" height="128">
    </picture>
    <h1 align="center" ">UNDEFINED</h1>
</p>
