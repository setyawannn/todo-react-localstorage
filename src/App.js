import React, { useEffect } from "react";
import moment from "moment/moment";
import "./App.css";
import { MdDelete } from 'react-icons/md'
import { MdDone } from 'react-icons/md'
import { MdEdit } from 'react-icons/md'
import Swal from "sweetalert2";


// Todo
function Todo({ todo, index, completeTodo, removeTodo, updateTodoItem }) {
  return (
    <div
      className="todo"
      style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
    >
      {todo.text}
      <div className="action">
        <button className="done" onClick={() => completeTodo(index)}><MdDone /></button>
        <button className="edit" onClick={() => updateTodoItem(index)}><MdEdit /></button>
        <button className="delete" onClick={() => removeTodo(index)}><MdDelete /></button>
      </div>
    </div>
  );
}

// Add todo
function TodoForm({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="form-control"
        placeholder="Add todo"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
    </form>
  );
}

// Get local storage
const getLocalStorage = () => {
  let todo = localStorage.getItem("todos");
  if (todo) {
    return (todo = JSON.parse(localStorage.getItem("todos")))
  } else {
    return []
  }
}

function App() {

  const [todos, setTodos] = React.useState(getLocalStorage);
  // Add local storage
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos))
  }, [todos]);

  // ADD TODO
  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  // COMPLETE TODO
  const completeTodo = index => {
    Swal.fire({
      position: 'top-center',
      icon: 'success',
      title: 'Yayyy tugas selesai',
      showConfirmButton: false,
      timer: 1300
    })
    const newTodos = [...todos];
    newTodos[index].isCompleted = true;
    setTodos(newTodos);
  };

  // REMOVE TODO
  const removeTodo = index => {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-danger  mx-2 px-4',
        cancelButton: 'btn btn-success mx-2 px-4'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Yakin ingin menghapus?',
      text: "Anda tidak dapat mengulanginya",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Terhapus',
          'Data anda telah di hapus',
          'success',
          removeTodo2(index)
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancel',
          'Data anda tidak terhapus :)',
          'error'
        )
      }
    })


  };

  const removeTodo2 = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  const updateTodoItem = (index) => {
    const newTodoItems = [...todos];

    let item = newTodoItems[index];

    // let newItem = prompt(`Update ke?`, item.text);
    Swal.fire({
      title: 'Ubah todo ?',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Ubah',
      showLoaderOnConfirm: true,

      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        newTodoItems.splice(index, 1, { text: result.value });

        if (result === null || result === "") {
          return;
        } else {
          item.text = result;
        }
        setTodos(newTodoItems);
        Swal.fire({
          title: "Berubah menjadi : " + result.value,
        })
      }
    })
  };

  // Local storage
  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem("todos")) || [];
    setTodos(storedTodos);
  }, []);

  return (
    <div div className="app" >
      <div className="todo-list">
        <div className="todo-title">
          <h3>Todo</h3>
          <p>{moment().format("MMM Do YY")}</p>
        </div>
        <TodoForm addTodo={addTodo} />
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
            updateTodoItem={updateTodoItem}
          />
        ))}
      </div>
    </div >
  );
}

export default App;
